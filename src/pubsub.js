// TODO to run this scalably - beyond a single server - replace PubSub with a persistent store such as Redis
const { PubSub } = require('apollo-server-express')
const pubsub = new PubSub()

const notify = () => {
  const now = new Date()
  pubsub.publish('NEW_MESSAGES', {
    messages: {
      node: {
        username: 'demo-user',
        message: `Hi, this is a new message sent at ${now.toUTCString()}`,
        id: now.toUTCString()
      },
      cursor: now.toUTCString()
    }
  })
}

// This is just for demo purposes
setInterval(notify, 3000)

module.exports = pubsub

const {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLString,
  GraphQLID
} = require('graphql')

const {
  connectionDefinitions,
  connectionArgs,
  connectionFromArray
} = require('graphql-relay')

const pubsub = require('../pubsub')

const newMessagesType = new GraphQLObjectType({
  name: 'newMessage',
  description: 'A new chat message',
  fields: () => ({
    id: {
      type: GraphQLNonNull(GraphQLID),
      resolve: (source, args, context, info) => {
        return Buffer.from(source.id).toString('base64')
      }
    },
    username: {
      type: GraphQLNonNull(GraphQLString),
      description: 'The name of the user that sent the message',
      resolve: (source, args, context, info) => {
        return source.username
      }
    },
    message: {
      type: GraphQLNonNull(GraphQLString),
      description: 'The message',
      resolve: (source, args, context, info) => {
        return source.message
      }
    }
  })
})

const subscriptionType = new GraphQLObjectType({
  name: 'Subscription',
  description: 'Subscribe to data event streams',
  fields: () => ({
    messages: {
      type: messageEdge,
      description: 'New chat messages',
      subscribe: (parent, args, context, info) => {
        return pubsub.asyncIterator(['NEW_MESSAGES'])
      }
    }
  })
})

const { connectionType: messageConnection, edgeType: messageEdge } = connectionDefinitions({ nodeType: newMessagesType })

function getMessages () {
  return [
    {
      id: '1',
      username: 'DemoUser1',
      message: 'Hello World'
    },
    {
      id: '2',
      username: 'Hunter2',
      message: 'Change your password'
    }
  ]
}

const queryType = new GraphQLObjectType({
  name: 'Query',
  description: 'Root query type',
  fields: () => ({
    messages: {
      type: messageConnection,
      description: 'Messages in the mock chatroom - note that pagination is not implemented since this is just a demo',
      args: connectionArgs,
      resolve: (root, args) => {
        return connectionFromArray(
          getMessages(),
          args
        )
      }
    }
  })
})

module.exports = new GraphQLSchema({
  query: queryType,
  subscription: subscriptionType
})

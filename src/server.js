const { ApolloServer } = require('apollo-server-express')
const express = require('express')
const schema = require('./schema')
const app = express()
const cors = require('cors')
const http = require('http')

const corsOptions = {
  origin: ['http://localhost:3000']
}

app.use(cors(corsOptions))

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort (val) {
  const port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

const apollo = new ApolloServer({ schema })

apollo.applyMiddleware({
  app,
  cors: corsOptions
})

const server = http.createServer(app)

// Add subscription support
apollo.installSubscriptionHandlers(server)

const port = normalizePort(4001)

server.listen({ port }, () => {
  console.log(`Server ready at localhost:${port}/graphql`)
})

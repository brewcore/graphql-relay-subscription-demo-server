# GraphQL Demo API

A demo server that allows subscriptions for new chat messages.

The server automatically generates new messages every 3 seconds for demo purposes.

To start the server:
- Install NodeJS 8.x
- Optional: Install yarn
- Run `yarn start` or `npm start` to start the server
- Server will be accessible at `http://localhost:4001/graphql`
